<?php

if ( ! defined( 'ABSPATH' ) )
	exit;

function wc_gzdp_order_needs_confirmation( $order_id ) {
	$order = ( is_object( $order_id ) ? $order_id : wc_get_order( $order_id ) ); 
	return wc_gzd_get_crud_data( $order, 'order_needs_confirmation' ) ? true : false;
}

function wc_gzdp_get_order_address_differing_fields() {
    return apply_filters( 'woocommerce_gzdp_order_address_differing_fields', array(
        'company',
        'first_name',
        'last_name',
        'address_1',
        'address_2',
        'city',
        'country',
        'postcode'
    ) );
}

function wc_gzdp_order_has_differing_shipping_address( $order ) {
    $order = ( is_numeric( $order ) ? wc_get_order( $order ) : $order );

    if ( is_callable( $order, 'has_shipping_address' ) ) {
        if ( ! $order->has_shipping_address() ) {
            return false;
        }
    } else {
        $address_1 = wc_gzd_get_crud_data( $order, 'shipping_address_1' );
        $address_2 = wc_gzd_get_crud_data( $order, 'shipping_address_2' );

        if ( ! $address_1 && ! $address_2 ) {
            return false;
        }
    }

    foreach( wc_gzdp_get_order_address_differing_fields() as $field ) {
        $b_data = wc_gzd_get_crud_data( $order, 'billing_' . $field );
        $s_data = wc_gzd_get_crud_data( $order, 'shipping_' . $field );

        if ( $b_data !== $s_data ) {
            return true;
        }
    }

    return false;
}