<?php

if ( ! defined( 'ABSPATH' ) )
    exit;

class WC_GZDP_Theme_Astra extends WC_GZDP_Theme {

    public function adjust_product_notices( $notices, $location ) {
        if ( 'loop' === $location ) {
            if ( isset( $notices['price_unit'] ) ) {
                $notices['price_unit']['filter'] = 'astra_woo_shop_price_after';
                $notices['price_unit']['priority'] = 5;
            }

            if ( isset( $notices['tax_info'] ) ) {
                $notices['tax_info']['filter'] = 'astra_woo_shop_price_after';
                $notices['tax_info']['priority'] = 6;
            }

            if ( isset( $notices['shipping_costs_info'] ) ) {
                $notices['shipping_costs_info']['filter'] = 'astra_woo_shop_price_after';
                $notices['shipping_costs_info']['priority'] = 7;
            }

            if ( isset( $notices['delivery_time_info'] ) ) {
                $notices['delivery_time_info']['filter'] = 'astra_woo_shop_price_after';
                $notices['delivery_time_info']['priority'] = 10;
            }

            if ( isset( $notices['product_units'] ) ) {
                $notices['product_units']['filter'] = 'astra_woo_shop_price_after';
                $notices['product_units']['priority'] = 15;
            }
        }

        return $notices;
    }
}